<?php

/**
 * @file
 * Ctools content type plugin that shows a Twitter Block.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'title' => t('Linkedin Group Discussions post'),
  'category' => t('Miscellaneous'),
  'render callback' => 'linkedin_groups_content_type_render',
  'defaults' => array(),
  'edit form' => 'linkedin_groups_content_type_edit_form',
);

/**
 * Render function for the block content.
 */

function linkedin_groups_content_type_render($subtype, $conf, $args) {
  $block = new stdClass();
  $block->content = linkedin_groups_get_group_data($conf['post_type'], $conf['linkedin_groups_id'], $conf['linkedin_groups_num_posts'] );
  return $block;
}

/**
 * Block configuration form.
 */
function linkedin_groups_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['settings']['linkedin_groups_fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Linkedin Group Configuration'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

  $form['settings']['linkedin_groups_fields']['linkedin_groups_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Group ID'),
      '#default_value' => $conf['linkedin_groups_id'],
      '#maxlength' => 64,
      '#description' => 'Please enter your group ID.',
      '#weight' => 2,
      '#access' => TRUE,
    );

    $form['settings']['linkedin_groups_fields']['post_type'] = array(
      '#type' => 'select',
      '#title' => t('Choose the type of posts.'),
      '#options' => array(
        'recency' => t('Recency'),
        'popularity' => t('Popularity'),
      ),
      '#default_value' => $conf['post_type'],
      '#weight' => 3,
      );

    $form['settings']['linkedin_groups_fields']['linkedin_groups_num_posts'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of Posts'),
      '#default_value' => $conf['linkedin_groups_num_posts'],
      '#maxlength' => 64,
      '#description' => 'Please enter how many posts do you want to display?',
      '#weight' => 4,
      '#access' => TRUE,
    );
  return $form;
}

/**
 * Block config submit handler.
 */
function linkedin_groups_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('linkedin_groups_id', 'post_type', 'linkedin_groups_num_posts') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Panels admin info function. Returns the info that you see in panels admin interface
 * once you have placed the block in a pane.
 */
function linkedin_groups_content_type_admin_info($subtype, $conf) {

  $block = linkedin_groups_content_type_render($subtype, $conf, array());
  $block->title = ' <strong>Something :) </strong>';

  return $block;
}
